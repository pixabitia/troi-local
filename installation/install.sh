##!/usr/bin/env bash
apt-get install python-software-properties
add-apt-repository ppa:ondrej/php
add-apt-repository ppa:webupd8team/java
apt update
apt install nginx php7.0-fpm php7.0-mcrypt php7.0-curl php7.0-gd php7.0-cli mysql-server php7.0-mysql php7.0-mbstring php7.0-xml php7.0-zip php7.0-sqlite3 php7.0-bcmath php-redis mysql-client redis-server ca-certificates openjdk-8-jre-headless

apt install redis-tools redis
